#!/bin/bash

if (( $# > 0 ))
then
  if [[ $1 == "DEBUG" ]]
  then
    DEBUG=1
    shift
  fi
fi

if [[ $DEBUG == 1 ]]
then
    ansible-playbook -v -i ../inventory.ini changeip.yml --check
else
    if (( $# < 1 ))
    then
      ansible-playbook -v -i ../inventory.ini changeip.yml
    else
      for host in $*
      do
        ansible-playbook -v -i ../inventory.ini changespecificip.yml --extra-vars "target=$host" --limit $host
      done
    fi
fi
