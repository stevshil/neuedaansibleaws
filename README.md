# neuedaansibleaws

This is the area that is used to configure the VMs once they are up and running.

# To run this code;

1. Ensure this directory is up to date first
  ```git reset --hard```
  ```git pull```
2. Copy the inventory.ini file up one level;
  ```cp -f inventory.ini ..```
3. Edit the ../inventory.ini and add the 172 IP addresses to the file.
    - These should be supplied from whom ever started the servers.
    - Change the 172.31.x.x to the relevant IP address
    - **Do not use the public IPs of the AWS hosts**
4. Run the following command
  ```./setup.sh```


# uDeploy and TeamCity servers

These 2 servers require further configuration through the Web interface, see the documentation for more detail.
