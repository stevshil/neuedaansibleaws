# Citi AWS Environment Set up

This document defines the process of launching the Citi bank AWS
training environment for the graduates to use.

**Table of content**

[TOC]


The process is a 3 stage process as follows;

1.  Start all VMs from the AMIs
    a.  This process will be performed by Nick Todd or Steve Shilling
    b.  They will provide the Private and Public IP addresses for each of the machine type
    c.  The OpenShift Application server will need Elastic IPs to retain their IP on reboot, and also a suitable DNS name
2.  Complete the set up by logging on to the Ansible server as user grads
    a.  You will create the inventory.ini file in /home/grads to reflect the IP addresses of the private IPs of the servers
3.  Instructor will complete the set up of TeamCity Server and uDeploy through web UI
    a.  TeamCity needs a valid 90 day unlimited license code
    b.  uDeploy needs to have the deployment environment


## Starting the environment

The following VMs need to be started with the associated sizes and security groups. The order of start up is not important as the Ansible server will configure the rest of the environment.
This section is relevant to the person who can log on to the AWS web console.

-   **Ansible Server**
    -   Started from **CitiAnsible** AMI **ami-f7f8fa1d**
    -   Minimum **t3a.micro**
    -   1 per regional set up
    -   Security Groups
        -   **sg-cd9457ab Internal All**
        -   **sg-07e1ec60 SSHOnly**
    -   This server should be used to SSH to all other servers as SSH is disabled from the outside world, except here.
    -   Name: **ansible.conygre.com**
-   **ELK Trainer**
    -   This server is used by PS to enable them to learn how to make dashboard and filter data in Kibana from an Example Apache Log file.
    -   Started from **ELK Trainer** AMI **ami-07aae8308c8d6aa3f**
    -   Minimum **t3a.medium**
    -   1 per region
    -   Security Groups
         -   **sg-07e1ec60 SSHOnly**
         -   **sg-917cece9 ELK**
    -   Name: **elktraining.conygre.com**
-   **Trouble Shooting Server**
    -   This server is used by Production Support to test the graduates ability to debug common scenarios.  It makes use of Docker containers to allow connectivity of the services as though they were individual servers, and the students will SSH to the containers through specific ports.
    -   Started from **Citi Troubleshooting Test Server** AMI **ami-0cbafb92e53a19e7b**
    -   Minimum **m5a.2xlarge**
    -   1 globally
    -   Security Groups
         -   **sg-cd9457ab Internal All**
         -   **sg-0310aefa3ca464863 AllAppPorts**
         -   **sg-07e1ec60 SSHOnly**
    -   Name: **tshoot.conygre.com**
-   **IBM uDeploy**
    -   Started from **uDeployReinstallable** AMI **ami-69bdbc83**
    -   Minimum **m5a.large**
    -   1 per region
    -   Security groups
        -   **sg-7f62f207 TeamCity**
        -   **sg-cd9457ab Internal All**
    -   Name: **udeploy.conygre.com**
-   **BitBucket**
    -   This requires each student to sign up for a free cloud account
-   **ITRS**
    -   Started from **ITRS 2019 - with examples** AMI **ami-01bf24ff3a73ab7d8**
    -   Minimum **m5a.large**
    -   1 per region
    -   Security groups
        -   **sg-ad4b8bcb ITRS**
    -   Name: **itrs.conygre.com**
-   **Linux Server**
    -   1 per region
    -   Should have a script that will configure the number of users
-   **Windows Student Desktop**
    -   1 per student
-   **ELK stack**
    -   Started from **ELK 2019** AMI **ami-0e3fb3cdafaff7480**
    -   Minimum of **m5a.xlarge**
    -   1 per region
    -   Security Groups
        -   **sg-cd9457ab Internall ALL**
        -   **sg-917cece9 ELK**
    -   With 2 netprobes on ports 7036 and 7037.
    - Disk space **100GB**
-   **Docker Registry**
    -   1 globally
    -   Started from **CitiDokerReg** AMI **ami-011b4be3f9cd5480c**
    -   Minimum of **t3a.micro**
    -   Disk size: **80GB**
    -   Security Groups
        -   **sg-cd9457ab Internall ALL**
    -   Name: **dockerreg.conygre.com**
-   **Application servers** with OpenShift, ITRS Netprobe, uDeploy agent and relay, Logstash
    -   Started from **OpenShift Citi Grads** AMI **ami-02a7ebd4766f1b859**
    -   Minimum **m5a.2xlarge** (the bigger the better depending on number of apps
    -   Disk size **50GB** - **100GB**
    -   2 per region (1 Test and 1 Prod)
    -   Security Groups
        -   **sg-cd9457ab Internal All**
        -   **sg-76d09a0e AppServer**
        -   **sg-0310aefa3ca464863 AllAppPorts**
        -   **sg-07e1ec60 SSHOnly**
    - On the **Configure Instance** screen **Advanced Details** add the following to the **User data**;
      ```
      #cloud-config
        runcmd:
           - echo "appsrvdev.conygre.com" >/srvname
      ```
      Change *appsrvdev* for the host you are configuring.  It should match the DNS entry for the EIP (Elastic IP that will need to be assigned to the server prior to running Ansible).
      It is possible to log on to these servers and re-run the configuration at any time and wipe all the data by doing;
      ```
      sudo service openshift clean
      ```
  -	**Mock Yahoo feed**
	- Started from **Mock Yahoo Finance Feed 2019** AMI **ami-083942208ee8f3177**
	- Size **t3a.nano**
	- 1 world wide
	- Security Groups
		- **sg-2c671e4b Port8080Only**
		- **sg-cd9457ab Internall ALL**
	- Tags
		- Name: **feed.conygre.com**
		- Role: classroom
		- customer: neueda
		- end_time_utc: 22
		- start_time_utc: 12
			- The times are NY, so will need to change for locations that require this.

### Emergency Solution to TeamCity

- **Dev App Server**
  - Started from **CitiDevAppServer** AMI **ami-0034ae903c5844687**
  - For locations where there is no PS track
  - TeamCity agent can use SSH Exec to this server
    - User: teamcity
    - Password: neueda2018
  - Security Groups
    - **sg-cd9457ab Internall ALL**
    - **sg-0310aefa3ca464863  AllAppPorts**
  - Size: **m5a.2xlarge**

### Other servers
As of 2019 everything is done within OpenShift, so if you need a TeamCity server or a Jenkins server (preferred now as of 2019) you should launch it within your development project.

### To finish the build

The Instructor or whom is to configure the environment will need the following private IP address information from you to create the /home/grads/inventory.yml file (where 172.31.x.x should be replaced with the private IPs of the hosts);

```
[ansible]
localhost ansible_connection=local npName=ansible npCount=0

[elk]
172.31.x.x npName=ELK npCount=6

[udeploy]
172.31.x.x npName=uDEPLOY npCount=6

[dockerreg]
172.31.x.x npName=DOCKER npCount=6

[itrs]
172.31.x.x numInstance=6 npName=ITRS npCount=6

[appsrv]
172.31.x.x  serverType=DEV npName=DEV npCount=6 openshiftState=clean udeployState=refresh
172.31.x.x  serverType=TEST npName=TEST npCount=6 openshiftState=clean udeployState=refresh
172.31.x.x  serverType=PROD npName=PROD npCount=6 openshiftState=clean udeployState=refresh

[feed]
172.31.x.x npName=FEED npCount=6

[all:vars]
ansible_ssh_user=ec2-user
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
ansible_sudo=true
ansible_become=true
ansible_become_user=root
ansible_ssh_private_key_file=/home/grads/.ssh/ansible.pem
```

**IMPORTANT**

The next step requires access to the Ansible server, so you need to ensure that the Trainer knows about the Ansible public IP address and has the grads key (at the end of this document). They will need to log on to the system using the **grads** username.

## Instructor set up

The next part of the set up can be done by any instructor, or the person who launches the systems. You will need to log on to the Ansible server using the Public IP address or hostname, the **grads** username and the ssh key at the end of this document.

ssh -i *keyFilename* grads@*publicHostName*

The *keyFilename* must be permissions 600.

Once you are logged on to the server you will need to change to the **neuedaansibleaws** directory and perform the following actions;

```
git reset --hard
git pull
cp inventory.ini ..
```

-   Edit the inventory.ini file and do the following;
    - Set the private IP address where you have **x.x**
    - Set the **npCount** value to the number of required Netprobes to run on the hosts
    - **openshiftState** should be initially set to **clean** for the first run of ansible and then changed to **running** once the environment is up.  This will prevent loss of data if you need to re-run ansible against the host
    - **udeployState** should be initially set to **refresh** for the first run of ansible and then changed to **running** to prevent data loss
-   Save the file
-   Run the following command from **neuedaansibleaws** directory
    ```
    ./setup
    ```

Checking the environment should consist of pointing your web browser at the relevant services to ensure they are there and
don’t require a restart;
-   [*http://*teamCityServerPublicIP*:8080*](http://teamCityServerPublicIP:8080)
-   [*http://*teamCityServerPublicIP*:8080*](http://teamCityServerPublicIP:8080)
-   [*https://*testappserver.conygre.com*:8443*](https://testappserver.conygre.com:8443)

### Possible Issues
-   AppServer
    -   uDeploy agent and relay
        -   Ansible should start these correctly, but if they do not show in uDeploy then you will need to start them
    -   Wrong domain name.  In the event you need a completely new server at any time run the following;
        ```
        sudo service openshift clean
        ```
        WARNING: This will lose all projects and data.
-   IBM uDeploy
    -   Log in to the web UI using admin/password
    -   Click **Resources** in the top tab
    -   Click **Agent Relays** in the sub-tab
        -   There should be an Online relay with the private IP of the AppServer
    -   Click the **Agents** sub-tab
        -   You should see TEST offline
        -   The reason it is off line is that the Server still needs some configuration

### Changing IP Addresses of OpenShift servers

The OpenShift servers should be set up with Elastic IP addresses and DNS names before running ansible;
- appsrvdev.conygre.com
- appsrvuat.conygre.com
- appsrvprod.conygre.com

This will ensure that your server will work correctly.

### uDeploy

This server will require you to install the example application pipeline deployment. The following instructions will take you through this;

#### Logging on and Setup

-   Point your web browser at the following;
    -   [*http://*udeploy.conygre.com*:8080*](http://udeploy.conygre.com:8080)
-   Log in with;
    -   Username: admin
    -   Password: password
        Once you have logged in you will need to then add the Demo application to the system through the Web UI and some of the Plugins. The following steps will help with this;
-   Add the localhost agent to the **Settings --&gt; System Settings** page
    ![SystemSettings](images/udeploy-settings1.png)
    Note the bottom of the above image where the pull down shows **localhost**.

    Whilst on this page you should also change the **External Agent URL** and **External User URL** from **localhost** to the AWS private address of this server.
-   Once you have configured the IP address and the agent you should be able to see the the **Resoruces**/**Agents** will show TEST and PROD as being online. If not you can restart them by hovering over the TEST or PROD and then click the **Action** button and say restart.  Then click the **Refresh** link at the bottom of the screen and they should show online.

#### Adding Plugins

-   You should also make sure that the plugins are up to date. The plugins are located on the server in the ec2-iuser directory. There is a folder in there that you should download to your local machine so that you can up load them through the UI. To do this;
    -   Click the **Settings** tab
    -   Select **Automation Plugins**
    -   Click the **Load Plugin** button and select the plugin to upload from where you downloaded them. Make sure you select the right plugin as some of them are for Source. The automation plugins should look as follows;
     ![AutomationPlugins](images/udeploy-autoplugins.png)
        -   Plugins that you will definitely need to update or install;
            -   [***FileUtils-64.970423.zip***](https://www.dropbox.com/s/1co1mi1ypa6i440/FileUtils-64.970423.zip?dl=0)
            -   [***UrbancodeVFS-30.972885.zip***](https://www.dropbox.com/s/7esrbpq35ec15st/UrbancodeVFS-30.972885.zip?dl=0)
            -   [***LinuxSystemTools-9.631844.zip***](https://www.dropbox.com/s/ct6zqxyywxl7cir/LinuxSystemTools-9.631844.zip?dl=0)
    -   Repeat for the **Source Configuration Plugins**
    ![SourceConfigurationPlugins](images/udeploy-sourceplugins.png)
        -   [***DockerSourceConfig-30.975156.zip***](https://www.dropbox.com/s/5omsxeoym8klwfj/DockerSourceConfig-30.975156.zip?dl=0)

#### Importing the Templates

The templates in this repository can be imported into uDeploy, and from there the Grads can create their components from the templates and their application from the templates too.

##### Importing Component templates

* Click **Components** main tab
* Click the **Templates** sub-tab
* Click the **Import Template** button
  * Select **Create Process**
  * Select **Choose file** and locate the component template to import (under the udeploy/files/uDeployJSON/Templates/Components)
    * Do all of these files
  * Click **Submit**
* Now go through each of the templates in uDeploy and set the passwords where required under the configurations

##### Importing the Application Template

* Click **Applications** main tab
* Click the **Templates** sub-tab
* Click **Import Application Template**
  * Generic Process Upgrade Type = Create Process
  * Resource Template Upgrade Type = Create Resource Template
  * Click **Choose file** and select the Deploy+To+OpenShift-ApplicationTemplate-v2.json
  * Click **Submit**


#### Importing Demo Application (deprecated)

This has now been deprecated, for the new Application and Component templates

-   You should then import your application workflow.
    -   Downloaded from;
        -   **[Deploy Demo Trade App OpenShift](../roles/udeploy/files/Deploy+London+Trades+App-OpenShift-nafal.json)**
    -   Once you have it on your local machine yo will need to import it into uDeploy
        -   Click the **Applications** tab
        -   Click the **Import Applications** button
        -   In the new pop-up window change all the pull downs to Create, as shown below;
         ![](images/udeploy-importapp.png)
        -   Click the **Choose file** button and locate the JSON file that you just downloaded at the beginning of this set of steps for importing the application
        -   Click the **Submit** button
            -   If you get an error, run through the same steps as sometimes it just works on the second attempt
-   Now you need to assign the application to an Agent.
    -   Click the **Resources** main tab
    -   Click **Create Top-Level Group** button
        -   Fill in the box as follows;
         ![](images/udeploy-createresource.png)
    -   Click Save
    -   A new line will appear on the screen and if you hover over thatline a button will appear called **Actions.** Click this button and select **Add Agent**
        -   In the pull down list select TEST
        -   Click the **Save** button
    -   A new line will appear on the screen with the TEST agent showing. Hover over this line and an **Actions** button will appear. Click the button and select **Add Component**
    -   Click the pull down next to **Component** and select **Demo App **![](images/udeploy-createresource02.png)
    -   Click the **Save** button
-   Your application now has an agent, and your view should look like;
    ![](images/udeploy-toplevel.png)
-   Check that the version has automatically imported;
    -   Click the **Components** main tab
    -   Click **Demo App**
    -   Click the **Versions** sub-tab
    -   You should see something similar to; ![](images/udeploy-importversion01.png)

**Trouble shooting the application import**
-   Bad plugin version
    -   If you see the following alert pop-up; ![](images/udeploy-noplugin.png)
    -   You will need to update the Automation plugin for File Utils.  Check the 2nd to last line to see which Plugin needs updating in the Automation Plugins screen.
