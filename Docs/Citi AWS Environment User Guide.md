# Citi AWS Environment User Guide

This document should be read by all trainers working on the Citi analyst training program.

**Table of content**

[TOC]

Once the Citi environment is up and running you should have a list of Public and Private IP addresses. If you’re lucky, Nick may have created some DNS names for the servers.

Production support information can be found at [Production Support documentation](Production-Support.md).

![AWS Environment](images/AWS-Environment.png)

The **public names** for the servers should be;

- ansible.conygre.com
  - You only need this system if you need to SSH to one of the other servers to fix a problem
- docker.conygre.com
  - This server contains images to be used by Openshift, and your developers should publish their docker containers to here
  - The docker server listens on port 5000, so your docker push commands should use docker.training.local:5000
- elk.conygre.com
  - http://elk.conygre.com:5601
    - The web console for monitoring
    - 9200 for logstash to point to
- elktraining.conygre.com
  - http://elktraining.conygre.com:5601
  - The training server for ELK dashboards, etc
- itrs.conygre.com
  - Connect client desktop to 55801 to 558?? where ?? would be the number of instances running
- dev?.conygre.com
  - Normally ? is servers 1 to 3 where there are more than one location running concurrently.
  - These servers are used during the training and for projects by the grads to perform continuous integration of the Docker containers and projects to be deployed in OpenShift.
- prod.conygre.com
  - https://prod.conygre.com:8443
    - Web UI to manage Openshift
  - Each service that you release to OpenShift will need its own public DNS name to route to it so that it can be seen over the Iternet.  Each location has a set of DNS names numbered 1 to 14 mapped to the prod or test server for the students projects;
    - Belfast
      - bf?.prod.conygre.com
    - Dublin
      - db?.prod.conygre.com
    - London
      - ln?.prod.conygre.com
    - Singapore
      - sg?.prod.conygre.com
- uat.conygre.com
  - https://uat.conygre.com:8443
    - Web UI to manage Openshift
  - Each service has its own DNS name, see prod.conygre.com, but subdomain will be uat.conygre.com
- udeploy.conygre.com
  - http://udeploy.conygre.com:8080
    - Web UI to manage uDeploy


The **internal names** are the same, but instead of **conygre.com** they end **training.local**.  The local names should be used when configuring applications, ITRS, uDeploy or TeamCity to talk to each other.

## Using SSH

To SSH to any of the servers you should use the Ansible host. Here’s an
example of how you would log on to the TEST AppServer;

-   ssh -i grads\_id.pem grads@**anisble.conygre.com**
-   ssh -i \~/.ssh/ansible.pem ec2-user@**testappserver.training.local**

    The Ansible server is a bastion or jump host that will allow you to log on and fix issues with any of the servers in the environment.

    The password for the key is **neueda**

## Web Interfaces

The following servers have a web interface that should be available through the Public IP address of the server;

-   uDeploy
    -   *http://udeploy.conygre.com:8080*
    -   Username: admin
    -   Password: password
-   App Servers
    -   *https://uat.conygre.com:8443*
    -   *https://prod.conygre.com:8443*
    -   Username: admin
    -   Password: admin
    - However, users can type their own name and password (good for projects) to seperate their view from other projects
-   ELK
    -   *https://elk.conygre.com:5601*
-   ITRS
    -   This is mainly accessed from the Windows Desktop
    -   You will need to launch the application from the folder on the Desktop
    -   Configure the gateway to point to the Private IP of the ITRS server
    -   It has some example samplers already configured, the focus is on making dashboards and monitoring the existing app
    -   This server does have webslinger and dashboard installed
    -   The Dashboard is only an example not a working one
    -   The idea is to create the dashboard on the desktop and then export it to the server
    -   Webslinger
        -   *http://itrs.conygre.com:8081*
             ![](images/webslinger01.png)
    -   Web Dashboard
        -   *http://itrs.conygre.com:8080*
             ![](images/itrsdashboard01.png)
-   Windows Desktop
    -   Username: Administrator
    -   Password: c0nygre
-   Linux training server with Docker, oc and kompose
    -   Username: citi
    -   Password: c0nygre

## ITRS by Region

Below are the ports relating to each region for ITRS to which you should point your desktop, or netprobes too.

NOTE: If you use the wrong port your will be kicked off by the other site for using there port, so make sure you use the correct one.

* Canada
  * Desktop points to Gateway on port 55801
  * ITRS Gateway configuration /opt/ITRS/gateway-01
  * Web Dashboard config /opt/ITRS/webdashboard
    * Access with http://itrs.conygre.com:8080
  * Web Slinger config /opt/ITRS/webslinger
    * Access with http://itrs.conygre.com:8081

* New York
  * Desktop points to Gateway on port 55802
  * ITRS Gateway configuration /opt/ITRS/gateway-02
  * Web Dashboard config /opt/ITRS/webdash-ny
    * Access with http://itrs.conygre.com:8090
  * Web Slinger config /opt/ITRS/ws-ny
    * Access with http://itrs.conygre.com:8091

* Singapore
  * Desktop points to Gateway on port 55803
  * ITRS Gateway configuration /opt/ITRS/gateway-03
  * Web Dashboard config /opt/ITRS/webdash-sg
    * Access with http://itrs.conygre.com:8070
  * Web Slinger config /opt/ITRS/ws-sg
    * Access with http://itrs.conygre.com:8071

* Belfast
  * Desktop points to Gateway on port 55804
  * ITRS Gateway configuration /opt/ITRS/gateway-04
  * Web Dashboard config /opt/ITRS/webdash-bf
    * Access with http://itrs.conygre.com:8060
  * Web Slinger config /opt/ITRS/ws-bf
    * Access with http://itrs.conygre.com:8061

* London
	* Desktop points to Gateway on port 55805
	* Web Dashboard config /opt/ITRS/webdash-ln
	* ITRS Gateway configuration /opt/ITRS/gateway-05
	  * Access with http://itrs.conygre.com:8050
	* Web Slinger config /opt/ITRS/ws-sg
	  * Access with http://itrs.conygre.com:8051

### Production Support

They also have an area split into regions, and a demo app that should be copied into the region and configured to work on the servers. This should be done as part of the course to show the students how they can do it and copy projects. You will also need to target your specific agent to build the project.

There are 2 demo projects;
-   Demo App JAR
    -   Building of the application as a Jar file and then packaging as a tgz file
-   Demo App Docker
    -   Builds the applciation JAR and then packages in a Docker container
    -   It then pushes to the Docker Registry server on completion

## uDeploy

To deploy the demo application to the App Server, TeamCity must have built the Docker images and published it to the Docker Registry server.
uDeploy will then import the version automatically (although this is a dummy import).

To deploy the application to the App server do the following;

-   Log on to uDeploy web UI
    -   *http://udeploy.conygre.com:8080*
-   Login with admin/password
-   Click the **Applications** main tab
-   Click **DemoApp**
-   Click the **Configuration** sub-tab
    -   Select **Application Properties** from the left side menu
    -   You will need to edit the IP addresses and the RELEASE value
        -   The RELEASE value enables multiple versions of the app to live in OpenShift (App Server)
-   Click the **Environments** sub-tab
-   Click the play button ![](images/udeploy-play.png) next to **TEST**
-   In the pop up window;\
    ![](images/udeploy-runprocess01.png)
    -   Click **Choose Versions**
    -   In the pop up window;
        ![](images/udeploy-componentversion01.png)
        -   Click the **Add...** Link
        -   In the pop up window click the drop down
            -   Click the version
            -   Click out of the area      
                ![](images/udeploy-componentversion02.png)
                The version now shows as a blue box
        -   Click the **OK** button
        -   The versions in the previous window will now show **1 selected**
        -   Click **Submit** button
            To add the PROD environment you should be able to make a
            copy of the existing application and change its name and the
            environment variables.

### Checking the App Deployment

You can check you Application deployment by going to the AppServers web UI;

-   *https://uat.conygre.com:8443*
-   *https://prod.conygre.com:8443*
    -   Username: admin
    -   Password: admin
-   You should see the name of your project as **demoapp** and the
    number you put in the RELEASE variable.
    -   Click on the project and you can start to view
    -   There should be a POD already started (normally a circle with the word POD in the middle and the number of instances running
    -   If you click on the circle you be taken to another view of which there is a link called **Terminal**. Click on the terminal and you’ll have a Linux CLI.

        You should also see logging to ELK server too, and the MySQL
        Trades database should start receiving data if you changed the
        IP addresses in the application configuration.

## Licenses

Some of the systems require licenses, of which the following will not last for the duration of the first to the last program;

* uDeploy
  - There is no way to change the license, so the server has to be rebuilt.  The license is approximately 30 days
* ITRS
  - This may last, if Citi are able to provide a key with the required length

### Changing ITRS License

This process will need to be done for every license key file.

* Change to the ITRS directory
  ```
  cd /opt/ITRS
  ```
* Under each gateway-0? directory is a file called **gateway2.lic.tmp** which needs to be modified.  Change the file to match the new values you have been given.  The format of a file is;
  ```
  D.35.94.A5.B5.24.C5.3D.F7
  S.90.A0.01.11.57.9D
  ```
* Restart ITRS;
  - ```
    sudo service itrs stop
    ```
  - ```
    sudo service itrs start
    ```

### Updating uDeploy License

At the moment there is no way to change a license for uDeploy.  Instead the software has to be re-installed, which means the data is lost.  Because of this you will need to backup your applications through the web UI before running the command to re-install, as follows;

* Log on to the web UI;
  - http://udeploy.conygre.com:8080
    - admin/password
* Click the **Applications** tab
* Tick the boxes of all the applications
* Click the **Actions** button and select **Export**
* Save the files to your disk
  - You will need these files after the software has been re-installed
  - You will need to modify the files to remove any secrets, since they will be encoded wrongly
* Follow these instructions:
  - https://bitbucket.org/stevshil/neuedaansibleaws/src/master/Docs/Citi%20AWS%20Environment%20Set%20up.md#uDeploySetup
  - Use your downloaded files when installing the applications

The default flow for the trade app can be found [here](../roles/udepoy/files/uDeployJSON/Application-Deploy+Trade+App.json)

## GRADS user private key

```
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,137F755030FE53AF875BC1479100A626

GJViJskk2RCNOmeUHn31WeVfLFrm/r6wJAK9MqzDqaiHsA+aUQR2d8bn3N6idlof
WND3oWQOiLVdZ4/1kHzZOlI8QI/fay9CgTTw1e3KicbDs0qa7KpnMC/pHbf1k7YN
gXRgkFoyZ0p+WBuhfyy+b1JC7CR7vqbKx2iCyYj8jFuZ5ojmz+q5VryAxCcTi8Sy
kK67fxDy+Z4XbDHX0dF7NAZVTK0nCUs20+I53h2s3tC1dMkwKcOW1YVv41qOaM6P
0bFJgh8HukNHyEHiTkPR3c5Qjub5fn4wVT8CvE5tJ50WbWBflxdQI/rKD1SxAAJW
F4S7MWUWg9NqMcG2LSQebw9pfUJ69szkpALYnmP9OCAMQ7V3Ul9azkM2Ozx7md+b
l3GQf0R6YhAoIrbY0TVcaJKTlH2g+r2GGBDHDCIaCSd1iqYETzhp0Sj+kSt2FEr0
Wx1QHkt/mGqwvpiKs27yg94RMiebXEJ6ffG5DHvWTgwetmya9wqhJkAAs9kzosCm
uwOCxNWhaQVgpJE/TOPeGD38aAnY/vgnWSmXJ9KfXvLMwDt1W+qfknvnT95yZCDq
AK1clAZpIWX2EpKdkGxze1ADCTbSIbRA9/HdraEM1Dvl5nxN4eQLVxFK2oh8d0zj
nCxy1UM4Zq0Wp81OT5MaO6tMy/7v1zCAJ9WluSEQa3vmAqvR9AU5dk2wzWEbxxot
wqJdsRLae7nCqOWTIrddz60etBs0tGFhEMvRahpvQeFHzPxezOya1ryVfrIDu8V2
J2Ddhk7Y1ul01jjpp/7xKDMjr7aHKoTsbCRMWf1X+YaUI9NxqXU0f7Z9uvvMk4Oc
RyXMG9Q1ox6C1DC2eRtTsgDNgNXTQTUa9u9iN1Ss6MFwFR6EdmLCTfE0/lyGO99r
7FF08SWUdP3sOvvUmxx2Pvak3nGuoM3GMI/hhTsMkYwNBAoHnS5alanaKbg4NT0E
lUc9UJk75eW1vYfZjvRLFphNYgVjm4dxRbyFHSeSpsPK080DQHkA6pug6lA30Bii
XLCIg4gjrR5ySZ2TqYYDxzmfPVCfBSbkQpXe99v2LJVSjkmrN85+fhnIW5bP2nBK
i+oWgTmvnYpoWXrZyCUJLNHeQw29KzTnqaWpgmT9Jjorqyp5r55II2FR4XHk2TMC
bBgo6E+dwPNL85qsAVOjX+FIUPpp8is7LCau5TjMUczGhnWLB3m0/qWK34UUaBFI
hlDlQJdccVLeYPe0HB47bHEwN8xgY4BwrIYjP63QusuNl0TxQMHKxQ1SoThnF2UW
71eHGXdS7zdT+Xuzh3V8/ALckuubSKGZ+Mu3cVuPyP6jt/q8jYgPinl4pGsfLmOM
p0nw5NcY5s6RzcgGoDaxNh9yKAFKedimwcGBF9R3vIKK4VD5XtaADaTTuG8E/Uar
ajQsaAdj1kIGJ06NMke2YmsbjDiXTZcVw8Ja1c8Agy67n40fgMje06q8QwfdVJ7L
ab0+DcjF8RyEBt0JgFdZKw65rGo9zgKouJZeofrrOG9GiBXCFPlbNaV9YzBqWVfK
5PcIzAdBWygDHY8eZsTbY8M+HHv+pf/Klv48J61Nz8kSX75AvkORrzf0d5cMKvnV
-----END RSA PRIVATE KEY-----
```
