# Reinstalling Servers

During the program some servers will need to be rebuilt, namely the uDeploy server, since it only has a 30 day license.

Other servers, such as ITRS you may wish to refresh your servers, e.g. if you finish Canada you'll want to refresh the configuration before starting in Singapore, or NY before Belfast and then London.

In these cases you do not need to destroy and recreate the servers, but simply run some scripts or commands on the actual servers.

## Refreshing uDeploy

1. Log on to **ansible.conygre.com** as the grads user (you'll need the grads ssh key and the password to unlock the key)
1. From here ssh to the **udeploy.training.local** server;
   ```ssh -i ~/.ssh/ansible.pem ec2-user@udeploy.training.local```
1. Once logged on run the following command;
   ```./uDeploy-install```
1. Once the script has completed check that the processes have started, it make take a few minutes to complete, but run the following to check;
   ```ps -ef | grep ucd```
   There should be 3 processes, 2 ibm-ucd/agent and 1 ibm-ucd/server.
   If after 15 minutes the processes are not there, or you cannot access the web site http://udeploy.conygre.com:8080 then run the following;
   ```sudo service ibmucd start```
   and then
   ```sudo service ibmucd-agent start```
1. Once you can log in to the uDeploy web interface you can proceed to the next section to configure the Application Servers.

### Completing the uDeploy configuration

Once uDeploy is installed and working you will need to add some plugins and the Application to the server.  Follow these steps;

1. Log in to the uDeploy web UI at http://udeploy.conygre.com:8080
   Use admin and password to log in.
1. Add the localhost agent to the **Settings --&gt; System Settings** page
    ![SystemSettings](images/udeploy-settings1.png)
    **Note** the bottom of the above image where the pull down shows **localhost**.
    Whilst on this page you should also change the **External Agent URL** and **External User URL** from **localhost** to the AWS private address of this server.
1. You should also make sure that the plugins are up to date. The plugins are located on the server in the ec2-iuser directory. There is a folder in there that you should download to your local machine so that you can up load them through the UI. To do this;
    -   Click the **Settings** tab
    -   Select **Automation Plugins**
    -   Click the **Load Plugin** button and select the plugin to upload from where you downloaded them. Make sure you select the right plugin as some of them are for Source. The automation plugins should look as follows;
     ![AutomationPlugins](images/udeploy-autoplugins.png)
        -   Plugins that you will definitely need to update or install;
            -   [***FileUtils-64.970423.zip***](https://www.dropbox.com/s/1co1mi1ypa6i440/FileUtils-64.970423.zip?dl=0)
            -   [***UrbancodeVFS-30.972885.zip***](https://www.dropbox.com/s/7esrbpq35ec15st/UrbancodeVFS-30.972885.zip?dl=0)
            -   [***LinuxSystemTools-9.631844.zip***](https://www.dropbox.com/s/ct6zqxyywxl7cir/LinuxSystemTools-9.631844.zip?dl=0)
    -   Repeat for the **Source Configuration Plugins**
    ![SourceConfigurationPlugins](images/udeploy-sourceplugins.png)
        -   [***DockerSourceConfig-30.975156.zip***](https://www.dropbox.com/s/5omsxeoym8klwfj/DockerSourceConfig-30.975156.zip?dl=0)
1. You should then import your application workflow.
    -   Downloaded from;
        -   *https://raw.githubusercontent.com/stevshil/vagrant/master/neueda/uDeployAppConfig/TradeApplicationDeployDocker-Application.json*
    -   Once you have it on your local machine yo will need to import it into uDeploy
        -   Click the **Applications** tab
        -   Click the **Import Applications** button
        -   In the new pop-up window change all the pull downs to Create, as shown below;
         ![](images/udeploy-importapp.png)
        -   Click the **Choose file** button and locate the JSON file that you just downloaded at the beginning of this set of steps for importing the application
        -   Click the **Submit** button
            -   If you get an error, run through the same steps as sometimes it just works on the second attempt
1. Now you need to assign the application to an Agent.
    -   Click the **Resources** main tab
    -   Click **Create Top-Level Group** button
        -   Fill in the box as follows;
         ![](images/udeploy-createresource.png)
    -   Click Save
    -   A new line will appear on the screen and if you hover over thatline a button will appear called **Actions.** Click this button and select **Add Agent**
        -   In the pull down list select TEST
        -   Click the **Save** button
    -   A new line will appear on the screen with the TEST agent showing. Hover over this line and an **Actions** button will appear. Click the button and select **Add Component**
    -   Click the pull down next to **Component** and select **Demo App **![](images/udeploy-createresource02.png)
    -   Click the **Save** button
1. Your application now has an agent, and your view should look like;
    ![](images/udeploy-toplevel.png)
1. Check that the version has automatically imported;
    -   Click the **Components** main tab
    -   Click **Demo App**
    -   Click the **Versions** sub-tab
    -   You should see something similar to; ![](images/udeploy-importversion01.png)


### Refreshing the Application Servers (OpenShift) for uDeploy

If after looking in **Resources** --> **Agents** in the uDeploy web UI, your agents all show **Error** then try the following steps;

1. Move the mouse over the Agents and click on the **Actions...** button
1. Select **Restart**
1. Refresh the screen after a minute and see if it has come **Online**
1. If it does repeat for the rest of the Agents

If the above doesn't work then we will need to re-install the agent on each of the OpenShift servers.  To do this perform the following steps;

1. In the uDeploy web UI remove all the agents, except **localhost**
1. Log on to **ansible.conygre.com** as grads
1. Edit the file **inventory.ini** in the grads home directory
1. Under the \[appsrv\] section change all the **udeployState=running** to **udeployState=refresh**
1. Change directory to **neuedaansibleaws**
1. Run the command;
   ```./setup.sh appsrv```
1. Once Ansible completes you should see the agents in uDeploy Online.

## Reinstalling an Application Server (OpenShift)

If you need to completely clear out an OpenShift server, then do the following on the existing server;

1. Log on to **ansible.conygre.com** as grads
1. Run the following command to completely clear the server to nothing;
   ```sudo service openshift clean```
