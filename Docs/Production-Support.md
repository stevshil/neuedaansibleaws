# Production Support Environment

As Production Support you must become familiar with working in a Production like environment.

We have created a simulated version of that environment in the Amazon Cloud, and it is shared across the globe and used by your colleagues in the different locations where training occurs.

You must therefore take care in this environment not to break something that is potentially in use by someone in another country.

This document contains information about ports, as well as being able to support the environment.

## Content

[TOC]

## The Saying

If you break it - you fix it!

## The environment

![AWS Environment](images/AWS-Environment.png)

## ITRS by Region

Below are the ports relating to each region for ITRS to which you should point your desktop, or netprobes too.

NOTE: If you use the wrong port your will be kicked off by the other site for using there port, so make sure you use the correct one.  The ports can always be checked by logging on to the ITRS gateway and checking the **gateway.setup.xml** file and looking at the listenPort at the bottom of the file for the gateway connection.

### Canada

* Desktop points to Gateway on port 55801
* ITRS Gateway configuration /opt/ITRS/gateway-01
* Web Dashboard config /opt/ITRS/webdashboard
  * Access with [[http://itrs.conygre.com:8080]]
* Web Slinger config /opt/ITRS/webslinger
  * Access with [[http://itrs.conygre.com:8081]]
* testserver Netprobe 7036
* prodserver Netprobe 7036
* elk Netprobe 7036
* uDeploy Netprobe 7036
* dockerreg Netprobe 7036
* teamcity Netprobe 7036
* TeamCity Agent = 172.31.32.204 Netprobe 7036
* activemq Netprobe 7036
* MySQL and Mongo Netprobe 55901


### New York

* Desktop points to Gateway on port 55802
* ITRS Gateway configuration /opt/ITRS/gateway-02
* Web Dashboard config /opt/ITRS/webdash-ny
  * Access with [[http://itrs.conygre.com:8090]]
* Web Slinger config /opt/ITRS/ws-ny
  * Access with [[http://itrs.conygre.com:8091]]
* testserver Netprobe 7037
* prodserver Netprobe 7037
* elk Netprobe 7037
* uDeploy Netprobe 7037
* dockerreg Netprobe 7037
* teamcity Netprobe 7037
* activemq Netprobe 7037
* TeamCity Agent = 172.31.32.204 Netprobe 7037
* MySQL and Mongo Netprobe 55902


### Singapore

* Desktop points to Gateway on port 55807
* ITRS Gateway configuration /opt/ITRS/gateway-03
* Web Dashboard config /opt/ITRS/webdash-sg
  * Access with [[http://itrs.conygre.com:8070]]
* Web Slinger config /opt/ITRS/ws-sg
  * Access with [[http://itrs.conygre.com:8071]]
* testserver Netprobe 7036
* prodserver Netprobe 7036
* elk Netprobe 7036
* uDeploy Netprobe 7036
* dockerreg Netprobe 7036
* teamcity Netprobe 7036
* activemq Netprobe 7036
* TeamCity Agent = 172.31.32.204 Netprobe 7036
* MySQL and Mongo Netprobe 55901


### Belfast

* Desktop points to Gateway on port 55808
* ITRS Gateway configuration /opt/ITRS/gateway-04
* Web Dashboard config /opt/ITRS/webdash-bf
  * Access with [[http://itrs.conygre.com:8060]]
* Web Slinger config /opt/ITRS/ws-bf
  * Access with [[http://itrs.conygre.com:8061]]
* testserver Netprobe 7037
* prodserver Netprobe 7037
* elk Netprobe 7037
* uDeploy Netprobe 7037
* dockerreg Netprobe 7037
* teamcity Netprobe 7037
* activemq Netprobe 7037
* TeamCity Agent = 172.31.32.204 Netprobe 7037
* MySQL and Mongo Netprobe 55902


### London

* Desktop points to Gateway on port 55805
* Web Dashboard config /opt/ITRS/webdash-ln
* ITRS Gateway configuration /opt/ITRS/gateway-05
  * Access with [[http://itrs.conygre.com:8050]]
* Web Slinger config /opt/ITRS/ws-sg
  * Access with [[http://itrs.conygre.com:8051]]
* testserver Netprobe 7036
* prodserver Netprobe 7036
* elk Netprobe 7036
* uDeploy Netprobe 7036
* dockerreg Netprobe 7036
* teamcity Netprobe 7036
* activemq Netprobe 7036
* TeamCity Agent = 172.31.32.204 Netprobe 7036
* MySQL and Mongo Netprobe 55901


When 2 sites are running concurrently instructors should communicate which one they are using.

## Logging on

To get to any system in the environment you will need to log on to the Ansible server.  You will need the ssh key to do this, and the relevant user.

## Docker

### Using a non-secure private registry

Any new systems that you start up that requrie access to the Docker registry, either to pull or push will need to have the following file added;

* /etc/docker/daemon.json

```
{
   "insecure-registries": ["docker.conygre.com:5000"]
}
```

Once you've added this file you will need to restart the Docker daemon.

```
service docker restart
```

### Cleaning up

Docker will get very busy over the course of a program, so it may need to be cleaned up.

On the Docker registry server there are 2 scripts;
* [getimage](../roles/dockerreg/files/getimage)
  - Used to list the images in the registry
* [delimage](../roles/dockerreg/files/delimage)
  - Used to remove an image from the Docker registry entirely.

Another way of cleaning up the registry is;
```
docker exec -it registry registry garbage-collect /etc/docker/registry/config.yml
```
