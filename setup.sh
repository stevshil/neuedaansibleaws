#!/bin/bash

if (( $# > 0 ))
then
  if [[ $1 == "DEBUG" ]]
  then
    DEBUG=1
    shift
  fi
fi

if [[ $DEBUG == 1 ]]
then
  if [[ $# > 0 ]]
  then
    ansible-playbook -v -i ../inventory.ini site.yml --check --limit $*
  else
    ansible-playbook -v -i ../inventory.ini site.yml --check
  fi
else
  if [[ $# > 0 ]]
  then
    ansible-playbook -v -i ../inventory.ini site.yml --limit $*
  else
    ansible-playbook -v -i ../inventory.ini site.yml
  fi
fi
